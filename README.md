Example
===
It takes one argument and runs it (detached process waits for a child to die):
```sh
$ detached "echo 'Hello World!'"
Hello World!
```

Dependencies
===
- [Stack](https://docs.haskellstack.org)

Build
===
```sh
stack build
```
Binary will be placed somewhere in `.stack-work` directory (e.g. `.stack-work/dist/x86_64-linux-tinfo6/Cabal-2.4.0.1/build/detached`)

Installation
===
To install package (putting it in your path, assuming correctly setup stack) run:
```sh
stack install
```

Run without installing
===
```sh
stack exec detached
```
or use stack-run package
```sh
stack run
```
