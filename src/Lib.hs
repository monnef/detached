module Lib
  ( runDetached
  ) where

import           Data.Function  ((&))
import           Data.Functor   (void)
import           System.Process

runDetached :: String -> IO ()
runDetached cmd = createProcess (shell cmd) {detach_console = True} >>= \(_, _, _, h) -> waitForProcess h & void
