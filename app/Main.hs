module Main where

import Lib

import System.Environment
import Data.Functor ((<&>))
import Control.Monad (when)
import Data.Function ((&))

main :: IO ()
main = do
  args <- getArgs
  when (length args /= 1) $ error "Expected exactly 1 argument - a shell command."
  head args & runDetached
